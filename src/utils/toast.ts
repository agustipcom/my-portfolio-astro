export function showToast(
  message: string,
  type: 'success' | 'error' = 'success',
  duration = 5000
) {
  const container = document.getElementById('toastContainer')
  if (!container) return

  const toast = document.createElement('div')
  toast.className = `flex px-4 py-2 rounded shadow text-black items-center gap-2 ${
    type === 'success' ? 'bg-green-400' : 'bg-red-400'
  } animate-fadeIn`

  const icon = document.createElement('i')
  icon.setAttribute(
    'data-feather',
    type === 'success' ? 'check-circle' : 'x-circle'
  )
  icon.className = 'toast-icon'

  const text = document.createElement('span')
  text.textContent = message

  toast.appendChild(icon)
  toast.appendChild(text)

  container.appendChild(toast)
  ;(window as any).feather.replace()

  setTimeout(() => {
    toast.classList.add('animate-fadeOut')
    toast.addEventListener('animationend', () => {
      toast.remove()
    })
  }, duration)
}
