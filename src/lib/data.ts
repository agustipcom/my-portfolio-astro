export interface Project {
  id: string
  title: string
  subtitle: string
  description: string
  gitUrl: string | null
  demoUrl: string | null
  images: string[]
  tags: string[]
  commingSoon?: boolean
}

export const projects: Project[] = [
  {
    id: '0',
    title: 'Sa Nostra Cuina',
    subtitle: 'Web Recipes',
    description:
      'A place to discover the recipes of a traditional Mallorcan cuisine. This project was developed using Next.js, Supabase, and Tailwind CSS. It is designed for users to explore the recipes of a local family, inherited from generation to generation.',
    gitUrl: null,
    demoUrl: null,
    images: ['project_snc_web_01.webp', 'project_snc_web_02.webp'],
    tags: ['NextJS', 'Supabase', 'Tailwind'],
    commingSoon: true
  },
  {
    id: '1',
    title: 'Cosmere Awards',
    subtitle: 'Web Astro',
    description:
      'A website built with Astro, simulating an award platform for the Cosmere universe.',
    gitUrl: null,
    demoUrl: 'https://cosmere-awards-web.vercel.app/',
    images: [
      'project_ca_web_01.webp',
      'project_ca_web_02.webp',
      'project_ca_web_03.webp'
    ],
    tags: ['Astro', 'Tailwind']
  },
  {
    id: '2',
    title: 'Hotel Gracia Pool Service',
    subtitle: 'Web & Tablet App',
    description:
      'This project involves developing a web platform and a mobile application using Flutter. The system is designed to enhance the hotel guest experience and optimize order management for the hotel staff.',
    gitUrl: null,
    demoUrl: null,
    images: [
      'project_hg_web_01.webp',
      'project_hg_web_02.webp',
      'project_hg_web_03.webp',
      'project_hg_app_01.webp',
      'project_hg_app_02.webp'
    ],
    tags: ['Flutter', 'Flutter Web', 'Dart', 'Firebase']
  },
  {
    id: '3',
    title: 'Javascript Quiz',
    subtitle: 'Web',
    description:
      'A simple quiz game of Javascript questions. Made with React and Typescript, it uses Zustand for state management and for persistancy. Material-UI for the UI components.',
    gitUrl: 'https://github.com/agustipc/js-quiz-zustand',
    demoUrl: 'https://javascriptquiz-99185.web.app/',
    images: [
      'project_jq_web_01.webp',
      'project_jq_web_02.webp',
      'project_jq_web_03.webp'
    ],
    tags: ['React', 'Typescript', 'Zustand', 'Material-UI, Vite']
  },
  {
    id: '4',
    title: 'Spotify Clone - Astro View Transitions Study',
    subtitle: 'Web - Chrome',
    description:
      'A Spotify clone built to test Astro view transitions. This project was developed using Astro, combined with React, Svelte, and Tailwind CSS. The songs are stored locally, and the player maintains its state while navigating between pages with smooth animated transitions.',
    gitUrl: null,
    demoUrl: null,
    images: ['project_spotify_01.webp', 'project_spotify_02.webp'],
    tags: [
      'Astro',
      'View Transitions',
      'Tailwind CSS',
      'React JS',
      'Typescript'
    ]
  },
  {
    id: '5',
    title: 'View Transitions Study',
    subtitle: 'Web - Chrome',
    description:
      'A study project focused on implementing view transitions for seamless navigation between pages. It explores animated transitions to enhance the user experience using Astro and Tailwind CSS.',
    gitUrl: null,
    demoUrl: null,
    images: [
      'project_libros_programacion_01.webp',
      'project_libros_programacion_02.webp'
    ],
    tags: ['Astro', 'View Transitions', 'Tailwind CSS']
  },
  {
    id: '6',
    title: 'Video Games CMS Study',
    subtitle: 'Web, CMS',
    description:
      'This project involved creating a content management system and website for video games. Developed with Next.js for the frontend and Strapi as a headless CMS, it served as a learning tool for building a complete CMS-based website.',
    gitUrl: null,
    demoUrl: null,
    images: ['project_video_games_01.webp', 'project_video_games_02.webp'],
    tags: ['Next.js', 'Strapi', 'CMS']
  },
  {
    id: '7',
    title: 'SpaceX Launches Study',
    subtitle: 'Web - Chrome',
    description:
      'A project aimed at learning Astro and Tailwind CSS, this website utilizes the SpaceX API to display the company’s launches. It serves as a case study for integrating external APIs and presenting dynamic content in an engaging layout.',
    gitUrl: null,
    demoUrl: null,
    images: ['project_spacex_01.webp'],
    tags: ['Astro', 'View Transitions', 'Tailwind CSS']
  }
]
